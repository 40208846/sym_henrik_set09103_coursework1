% #######################################
% ########### FILL THESE IN #############
% #######################################
\def\mytitle{Coursework Report}
\def\mykeywords{Phones, Webapp, Coursework}
\def\myauthor{Henrik Sym}
\def\contact{40208846@live.napier.ac.uk}
\def\mymodule{SET09103}
% #######################################
% #### YOU DON'T NEED TO TOUCH BELOW ####
% #######################################
\documentclass[10pt, a4paper]{article}
\usepackage[a4paper,outer=1.5cm,inner=1.5cm,top=1.75cm,bottom=1.5cm]{geometry}
\twocolumn
\usepackage{graphicx}
\graphicspath{{./images/}}
%colour our links, remove weird boxes
\usepackage[colorlinks,linkcolor={black},citecolor={blue!80!black},urlcolor={blue!80!black}]{hyperref}
%Stop indentation on new paragraphs
\usepackage[parfill]{parskip}
%% Arial-like font
\IfFileExists{uarial.sty}
{
    \usepackage[english]{babel}
    \usepackage[T1]{fontenc}
    \usepackage{uarial}
    \renewcommand{\familydefault}{\sfdefault}
}{
    \usepackage{helvet}
    \renewcommand*\familydefault{\sfdefault}
}
%Napier logo top right
\usepackage{watermark}
%Lorem Ipusm dolor please don't leave any in you final report ;)
\usepackage{lipsum}
\usepackage{xcolor}
\usepackage{listings}
%give us the Capital H that we all know and love
\usepackage{float}
%tone down the line spacing after section titles
\usepackage{titlesec}
%Cool maths printing
\usepackage{amsmath}
%PseudoCode
\usepackage{algorithm2e}

\titlespacing{\subsection}{0pt}{\parskip}{-3pt}
\titlespacing{\subsubsection}{0pt}{\parskip}{-\parskip}
\titlespacing{\paragraph}{0pt}{\parskip}{\parskip}
\newcommand{\figuremacro}[5]{
    \begin{figure}[#1]
        \centering
        \includegraphics[width=#5\columnwidth]{#2}
        \caption[#3]{\textbf{#3}#4}
        \label{fig:#2}
    \end{figure}
}

\lstset{
	escapeinside={/*@}{@*/}, language=C++,
	basicstyle=\fontsize{8.5}{12}\selectfont,
	numbers=left,numbersep=2pt,xleftmargin=2pt,frame=tb,
    columns=fullflexible,showstringspaces=false,tabsize=4,
    keepspaces=true,showtabs=false,showspaces=false,
    backgroundcolor=\color{white}, morekeywords={inline,public,
    class,private,protected,struct},captionpos=t,lineskip=-0.4em,
	aboveskip=10pt, extendedchars=true, breaklines=true,
	prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
	keywordstyle=\color[rgb]{0,0,1},
	commentstyle=\color[rgb]{0.133,0.545,0.133},
	stringstyle=\color[rgb]{0.627,0.126,0.941}
}

\thiswatermark{\centering \put(336.5,-38.0){\includegraphics[scale=0.8]{logo}} }
\title{\mytitle}
\author{\myauthor\hspace{1em}\\\contact\\Edinburgh Napier University\hspace{0.5em}-\hspace{0.5em}\mymodule}
\date{}
\hypersetup{pdfauthor=\myauthor,pdftitle=\mytitle,pdfkeywords=\mykeywords}
\sloppy
% #######################################
% ########### START FROM HERE ###########
% #######################################
\begin{document}
    \maketitle
    \section{Introduction}
    The web app is a collection of phones from a variety of manufacturers and
    with a variety of specifications, e.g. screen size, operating system. 
    The collection of phones can be browsed through in several ways, 
    from a list of all phones, by manufacturer, or by specification.
    For each phone a list of specifications, as well as an image of
    the phone is shown. There is also basic search functionality.
    Inspiration for the web app came from popular websites such as 
    GSMArena\footnote{https://www.gsmarena.com/}. 
	
    \section{Design}
    The web app runs on a Python\footnote{https://www.python.org/} 
    Flask\footnote{http://flask.pocoo.org/} server with 
    Jinja2\footnote{http://jinja.pocoo.org/} templates. The server uses a
    Python file to decide how to respond to HTML requests that the user's 
    browser sends. If a matching route is defined then the appropriate page
    is returned. If the route does not exist then a \emph{404} error is 
    returned and a suitable error page is displayed.
    The web app is designed so that all phones can be browsed from
    \mbox{\emph{/browse}}, or all phones from a certain manufacturer from 
    \mbox{\emph{/browse/Manufacturer}}
    -- where Manufacturer is the name of the manufacturer.
    See Figure~\ref{fig:browse} and ~\ref{fig:manufacturer}.
    For each of these a list of matching phones is created from a JSON file. 
    This list is then
    used to fill a Jinja2 template using Python statements embedded in HTML.
    This allows the same template to be used for both 
    types of list. Phones can also be browsed by specification, for example, 
    by operating system. The list is populated in the same fashion as the 
    list of all phones, but using different attributes. Furthermore the 
    list of specifications is sorted, where it is logical to do so -- for
    example screen sizes. 
    
    The URL hierarchy is such that each phone's information is
    at the location \mbox{\emph{/Manufacturer/Model/}} -- 
    shown in Figure~\ref{fig:phone} -- this was 
    chosen as it is clear and unique for each phone. On the information page
    for each phone an image, the name of the phone, and the phone's 
    specifications are displayed. The specifications are displayed as a table.
    For some specifications there are multiple entries, for these the label
    cell in the column expands to the size of the corresponding information
    cells.

    The data holding the 
    phones' specifications is held in a JSON file. This allows for new 
    phones to be easily added. Images for each phone are held in the static
    folder, along with the CSS for the pages. A basic form of search is 
    implemented using a combination of javascript and Python. If the typed 
    search string exactly matches the manufacturer and model or a phone
    then the relevant page is displayed, otherwise an error page is shown.
    
    The web app also makes use of Bootstrap and JQuery, 
    for the appearance of the HTML and to allow interaction respectively. 
    There has been light customisations made to the default Bootstrap theme.
    JQuery is used for the search box functionality.
    
    \section{Enhancements}
    One way the web app could have been improved would be filters for the 
    browsing page. If there were a large number of phones in the list then
    filtering by specification would be helpful. While some functionality like
    this is available, it only separates by specification rather than 
    filtering.

    The order of specifications is another area of possible future improvement.
    Currently the order the specifications are 
    displayed in is the order Python uses when converting the JSON file to 
    a dictionary. If the specifications were grouped together logically, i.e. 
    dimensions and screen size being together. The display of the 
    specifications could also be improved. 
    
    Another enhancement would be if the search could list 
    phones matching the inputted string rather than the exact name of a phone
    having to be entered. In addition, search suggestions while the user 
    inputs the search query would be an improvement.
    Finally the appearance of the web app could be improved as it currently 
    does not expand greatly on the default Bootstrap style. This could be done
    through finding and using an alternative Bootstrap theme or by creating
    more custom CSS styles. 

    \section{Critical Evaluation}
    The web app works well for the purpose of browsing various phones. The site 
    has high contrast and is easily readable. However the style of the web app
    is bland and uninspired, for example see Figure~\ref{fig:home}, 
    more time spent learning about Bootstrap theming 
    would have greatly helped. The various methods of browsing
    all work effectively and are discoverable as they are clearly distinct
    from the plain text. Navigation through the various
    pages of the site is easy as the layout is logical and interactive 
    elements are clear. The web app has a consistent navigation bar at the top
    of each page, meaning the user shouldn't get lost and can always return to 
    the home or browsing pages. 
    The display of each phone's information is adequate, 
    but the improvements suggested above would definitely make the web app as
    a whole more useful.
    The search functionality is basic but present. Improving the search 
    functionality would have involved too much work outside the scope of the
    learning objectives of the coursework and the module.
    
    \section{Personal Evaluation}
    The completion of this coursework taught a number of things, the
    first being that Flask is easy to use for the purposes of creating small
    web apps and is easily extensible via various plugins. Much of the learning
    came through the challenges faced and the steps to overcome them.
    One challenge that was faced was how to properly load and subsequently 
    access the data held in the JSON file. In order to solve this problem a 
    combination of internet research and trial-and-error were used. 
    Another 
    small problem was encountered when creating the search functionality, as it
    involved using Javascript in order to get the user input. 
    Therefore prior works using Javascript were 
    consulted in order to revise how to use the language. Much reference was
    made to various HTML documentation websites, for example, to make the
    label cells in the specification table expand properly.
    
    Overall performance for this coursework was fairly good, with appropriate
    time and effort given to accomplishing the objectives set out in the 
    descriptor.
    %\section{References}
    %\bibliographystyle{ieeetr}
    %\bibliography{references}
    \newpage
    \clearpage
    \appendix
    \section{Screenshots}

    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{Home.png}
        \caption{Home page}
        \label{fig:home}
    \end{figure}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{All_phones.png}
        \caption{Browsing all phones}
        \label{fig:browse}
    \end{figure}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{By_manufacturer.png}
        \caption{Browsing by manufacturer}
        \label{fig:manufacturer}
    \end{figure}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{By_specification.png}
        \caption{Browsing by specification}
        \label{fig:specification}
    \end{figure}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{By_screen_size.png}
        \caption{Browsing by specification, showing screen size section}
        \label{fig:specification2}
    \end{figure}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{Phone_information.png}
        \caption{Individual phone's information page}
        \label{fig:phone}
    \end{figure}
\end{document}
