from flask import Flask, render_template, abort, request
import json
from pprint import pprint
app = Flask(__name__)

app.jinja_env.add_extension('jinja2.ext.do')

# Get the phone data from the JSON file
with open('static/phones.json') as phoneJSON:
    phones_all = json.load(phoneJSON)

# The root of the web app
@app.route('/')
def root():
    return render_template('index.html')

# The page for browsing either all phones of all phones from a manufacturer
@app.route('/browse')
@app.route('/browse/<manufacturer>')
def browse(manufacturer=None):
    phoneList = []
    if manufacturer:
        for item in phones_all["phone"]:
            if item["Manufacturer"] == manufacturer:
                phoneList.append({"Manufacturer":item["Manufacturer"] , 
                    "Model":item["Model"]})
        if len(phoneList) == 0:
            abort(404)
    else:
        for item in phones_all["phone"]:
            phoneList.append({"Manufacturer":item["Manufacturer"] , 
                "Model":item["Model"]})
    return render_template('browse.html', 
            data=phoneList, manufacturer=manufacturer)

# Page for choosing phones by their specification
@app.route('/spec')
def spec():
    osList = []
    scrSizeList = []
    for item in phones_all["phone"]:
        osList.append({"OS":item["Operating system"] , 
            "Manufacturer":item["Manufacturer"], "Model":item["Model"]})
        scrSizeList.append({"ScrSize":item["Display size"] ,
            "Manufacturer":item["Manufacturer"], "Model":item["Model"]})
    scrSizeList.sort(key=lambda tup: tup["ScrSize"][:-1])
    return render_template('spec.html', OS=osList, ScrSize=scrSizeList)

# Individual info pages for each phone model, if the address does not exist,
# a 404 response is generated
@app.route('/<manufacturer>/<model>')
def infopage(manufacturer, model):
    for item in phones_all["phone"]:
        if item['Manufacturer'] == manufacturer:
            if item['Model'] == model:
                return render_template('infopage.html', phone=item)
    abort(404)

# Runs when search button is clicked, if a phone matches the inputted string
# the relevant page is shown
@app.route('/search')
def search():
    query = request.args.get('query','')
    if query == '':
        return render_template('index.html')
    else:
        manufacturer = query.split()[0]
        model = query[len(manufacturer)+1:] 
        print(manufacturer, model)
        for item in phones_all["phone"]:
            if item['Manufacturer'] == manufacturer:
                if item['Model'] == model:
                    return render_template('infopage.html', phone=item)
        abort(404)

@app.errorhandler(404)
def page_not_found(e):
        return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)

